<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $table = 'pagos';
    public $timestamps = false;
    protected $fillable = ['monto', 'status', 'tipoPago', 'referencia', 'fechaPago', 'statusAprobacion', 'banco'];
}
