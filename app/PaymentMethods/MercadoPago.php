<?php
/**
 * Created by PhpStorm.
 * User: ultron
 * Date: 22/01/2020
 * Time: 01:18 PM
 */

namespace App\PaymentMethods;

use App\OrdenView;
use MercadoPago\SDK;
use Illuminate\Http\Request;
use MercadoPago\Item;
use MercadoPago\MerchantOrder;
use MercadoPago\Payer;
use MercadoPago\Payment;
use MercadoPago\Preference;

class MercadoPago
{
    public function __construct()
    {
        SDK::setClientId(
            config("payments-methods.mercadopago.client")
        );
        SDK::setClientSecret(
            config("payments-methods.mercadopago.secret")
        );
    }

    public function setupPaymentAndGetRedirectURL(OrdenView $order): string
    {
        # Create a preference object
        $preference = new Preference();

        # Create an item object
        $item = new Item();
        $item->id = $order->id;
        $item->title = 'sneakers store';
        $item->quantity = 1;
        $item->currency_id = 'MXN';
        $item->unit_price = $order->total;
//        $item->picture_url = $order->featured_img;

        # Create a payer object
        $payer = new Payer();
        $payer->email = $order->emailCliente;

        # Setting preference properties
        $preference->items = [$item];
        $preference->payer = $payer;

        # Save External Reference
//        $preference->external_reference = $order->id;
        $preference->back_urls = [
            "success" => route('thanks'),
//            "pending" => route('checkout.pending'),
            "failure" => route('fail'),
        ];

        $preference->auto_return = "all";
//        $preference->notification_url = route('ipn');
        # Save and POST preference
        $preference->save();

        if (config('payments-methods.use_sandbox')) {
            return $preference->sandbox_init_point;
        }

        return $preference->init_point;
    }
}
