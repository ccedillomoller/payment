<?php

namespace App\Http\Controllers;

use App\OrdenView;
use App\Pago;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PagoController extends Controller
{
    public function pagoLink($total, $oid)
    {
        $totalTrans = base64_decode($total);
        $idTransac = base64_decode($oid);
        date_default_timezone_set('America/Mexico_City');
        $dateTime = date("Y:m:d-H:i:s");
        $storeId = "3900193";
        $currency = '484';
// NOTE: Please DO NOT hardcode the secret in that script. For example read it from a database.
        $sharedSecret = "eHLc_2;3qq";
        $stringToHash = $storeId . $dateTime . $totalTrans . $currency . $sharedSecret;
        $ascii = bin2hex($stringToHash);
        $hash = hash("sha256", $ascii);
        return view('pago-link', compact('totalTrans', 'idTransac', 'dateTime', 'hash'));
    }

    public function thanks(Request $request)
    {
        $monto = str_replace(",", ".", $request->chargetotal);
        Pago::create(['monto' => $monto, 'status' => $request->status, 'tipoPago' => 'Credito/Debito',
            'referencia' => $request->oid, 'fechaPago' => $request->txndate_processed, 'statusAprobacion' => $request->approval_code,
            'banco' => $request->ccbrand]);
        return view('thanks', compact('request'));
    }

    public function fail(Request $request)
    {
        $monto = str_replace(",", ".", $request->chargetotal);
        Pago::create(['monto' => $monto, 'status' => $request->status, 'tipoPago' => $request->paymentMethod,
            'referencia' => $request->oid, 'fechaPago' => Carbon::parse($request->txndate_processed)->toDateTime(),
            'statusAprobacion' => $request->approval_code, 'banco' => $request->ccbrand]);
        return view('fail', compact('request'));
    }

    public function mercadoPago($idOrden)
    {
        $order = OrdenView::find($idOrden);
        $url = $this->generatePaymentGateway(
            $order
        );
        return redirect()->to($url);
    }

    protected function generatePaymentGateway(OrdenView $order): string
    {
        $method = new \App\PaymentMethods\MercadoPago;

        return $method->setupPaymentAndGetRedirectURL($order);
    }
}
