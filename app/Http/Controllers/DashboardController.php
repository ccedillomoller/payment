<?php

namespace App\Http\Controllers;

use App\Pago;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    public function index()
    {
        $totalRechazado = 0;
        $totalAprovado = 0;
        $pagosRechazados = Pago::where('statusAprobacion', 'like', 'N%')->get();
        $pagosAprovados = Pago::where('statusAprobacion', 'like', 'Y%')->get();
        $pagos = Pago::get();
        foreach ($pagosRechazados as $pagosRechazado) {
            $totalRechazado += $pagosRechazado->monto;
        }
        foreach ($pagosAprovados as $pagosAprovado) {
            $totalAprovado += $pagosAprovado->monto;
        }
        return view('dashboard', compact('totalAprovado', 'totalRechazado', 'pagos'));
    }
}
