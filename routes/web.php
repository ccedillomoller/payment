<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    Route::get('generar-link', function () {
        return view('generar-link');
    })->name('generar-link');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('registrar-usuario', function () {
        return view('registrar-usuario');
    })->name('registrar-usuario');
});
Route::get('/', function () {
    return view('index');
});
Route::get('pago-link/{total}/{oid}', 'PagoController@pagoLink')->name('pago-link');
Route::match(['get', 'post'], 'thanks', 'PagoController@thanks')->name('thanks');
Route::get('fail', 'PagoController@fail')->name('fail');
Route::get('thanks', 'PagoController@thanks')->name('thanks');
Route::match(['get', 'post'], 'fail', 'PagoController@fail')->name('fail');
Route::get('mercadoPago/{idOrden}', 'PagoController@mercadoPago')->name('mercadoPago');

Auth::routes();

