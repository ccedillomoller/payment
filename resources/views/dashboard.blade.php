@extends('layout')
@section('title')
    Dashboard
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    Pagos aprobados
                </div>
                <div class="x_content">
                    ${{$totalAprovado}}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="x_panel">
                <div class="x_title">
                    Pagos rechazados
                </div>
                <div class="x_content">
                    ${{$totalRechazado}}
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-5">
        <div class="col-md-12">
            <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">
                    Transaciones
                </div>
                <div class="x_content">
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap"
                           cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Referencia</th>
                            <th>Total</th>
                            <th>Fecha de pago</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pagos as $pago)
                            <tr>
                                <td>{{$pago->referencia}}</td>
                                <td>{{$pago->monto}}</td>
                                <td>{{$pago->fechaPago}}</td>
                                <td>{{$pago->statusAprobacion}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('highcharts/code/highcharts.js')}}"></script>
    <script src="{{asset('highcharts/code/modules/exporting.js')}}"></script>
    <script src="{{asset('highcharts/code/modules/export-data.js')}}"></script>
    <script type="text/javascript">
        var aprobado =
                {!! json_encode($totalAprovado) !!}
        var rechazado = {!! json_encode($totalRechazado) !!}
            Highcharts.chart('container', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Total de Pagos'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    name: 'Total',
                    colorByPoint: true,
                    data: [{
                        name: 'Aprobado',
                        y: aprobado
                    }, {
                        name: 'Rechazado',
                        y: rechazado
                    }]
                }]
            });
    </script>
@endsection
