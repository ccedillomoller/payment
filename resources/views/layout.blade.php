<!DOCTYPE html>
<html lang="en">
@include('core.head')

<body class="nav-md">
<div class="container body">
    <div class="main_container">
    @include('core.sidebar')

    <!-- top navigation -->
    @include('core.top-nav')
    <!-- /top navigation -->
        <div class="right_col" role="main">
            <div class="page-title">
                <div class="title_left">
                    <h3>@yield('title')</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            @yield('content')
        </div>


        <!-- /page content -->

        <!-- footer content -->
    @include('core.footer')
    <!-- /footer content -->
    </div>
</div>

@include('core.scripts')

</body>
</html>
