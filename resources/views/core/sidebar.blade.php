<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{route('dashboard')}}" class="site_title"> <span>INSTAPAY</span></a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile clearfix">
            <div class="profile_info">
                <span>Bienvenido,</span>
                <h2>{{\Illuminate\Support\Facades\Auth::user()->name}}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>Menu</h3>
                <ul class="nav side-menu">
                    <li><a href="{{route('dashboard')}}"><i class="fa fa-laptop"></i>Dashboard</a></li>
                    <li><a href="{{route('generar-link')}}"><i class="fa fa-laptop"></i>Generar Link de Pago</a></li>
                    {{--<li><a href="{{route('registrar-usuario')}}"><i class="fa fa-laptop"></i>Registrar Usuario</a></li>--}}
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <!-- /menu footer buttons -->
    </div>
</div>
