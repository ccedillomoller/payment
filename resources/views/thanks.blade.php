@extends('layouts.app)
@section('customStyle')
    <link rel="stylesheet" href="{{asset('build/css/sb-admin-2.css')}}">
@endsection
@section('content')
    <nav class="navbar navbar-light bg-light justify-content-between">
        <a class="navbar-brand"><img src="{{asset('images/insta_logo.jpg')}}" alt="" class="img-fluid" width="30%"></a>
    </nav>
    <div class="container">
        <div class="row mt-5 pb-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Felicidades, su transacción ha sido completada</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                Forma de pago: <? echo $_POST['paymentMethod'] ?> <br>
                                                Fecha: <?php echo $_POST['txndate_processed'] ?> <br>
                                                N° de orden: <?php echo $_POST['oid'] ?>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                        <div class="row justify-content-between mt-2">
                                            <a href="https://sneakerstore.com.mx" class="btn btn-primary btn-lg">Regresar
                                                a
                                                tienda</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 mt-2 mt-md-0 mt-lg-0">
                                <div class="card">
                                    <div class="card-body">
                                        <p>
                                            Tu pedido
                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                Subtotal: <?php echo $_POST['chargetotal'] ?><br>
                                                Total: <?php echo $_POST['chargetotal'] ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <p>
                                        <i class="fas fa-lock"></i>
                                        Esta es una pagina segura que utiliza SSL/TLS (Secure Socket Layer/
                                        Transport Layer Security) para cifrar y transmitir sus datos de pago de una
                                        forma
                                        segura.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
