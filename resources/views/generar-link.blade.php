@extends('layout')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="x_panel">
                <div class="x_title">Generar Link</div>
                <div class="x_content">
                    <div class="form-group">
                        <label for="oid">Referencia</label>
                        <input type="text" class="form-control" id="oid" name="oid">
                    </div>
                    <div class="form-group">
                        <label for="monto">Monto</label>
                        <input type="text" class="form-control" id="monto" name="monto">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" onclick="generarUrl()">
                            Generar Link
                        </button>
                    </div>
                    <p id="url"></p>
                </div>
            </div>
        </div>
    </div>
    <script>
        function generarUrl() {
            var monto = document.getElementById('monto').value;
            var oid = document.getElementById('oid').value;
            var total_encode = btoa(monto);
            var oid_encode = btoa(oid);
            document.getElementById("url").innerHTML = 'https://payment.sneakerstore.com.mx/pago-link/' + total_encode + '/' + oid_encode;
        }
    </script>
@endsection

