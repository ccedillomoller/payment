@extends('layouts.app')
@section('customStyle')
    <link href="{{asset('build/css/sb-admin-2.css')}}" rel="stylesheet">
@endsection
@section('content')
    <nav class="navbar navbar-light bg-light justify-content-between">
        <a class="navbar-brand"><img src="{{asset('images/insta_logo.jpg')}}" alt="" class="img-fluid" width="30%"></a>
    </nav>
    <div class="container">
        <div class="row mt-5 pb-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="card">
                                    <div class="card-body">
                                        <p>Detalles de tu compra</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>
                                                    <b>Total de tu compra:</b> {{$totalTrans}}
                                                </p>
                                                <p>
                                                    <b>Numero de orden:</b> {{$idTransac}}
                                                </p>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 mt-2 mt-md-0 mt-lg-0">
                                <div class="card">
                                    <div class="card-body">
                                        <p>
                                            Forma de pago
                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <a href="{{route('mercadoPago',$idTransac)}}" class="btn btn-primarya btn-sm">Mercado pago</a>
                                                </div>
                                                <div class="row mt-3">
                                                    <button class="btn btn-primary btn-sm">
                                                        Pago en efectivo
                                                    </button>
                                                </div>
                                                <div class="row mt-3">
                                                    <form method="post"
                                                          action="https://www2.ipg-online.com/connect/gateway/processing">
                                                        <input type="hidden" name="txntype" value="sale">
                                                        <input type="hidden" name="timezone"
                                                               value="America/Mexico_City"/>
                                                        <input type="hidden" name="txndatetime"
                                                               value="{{$dateTime}}"/>
                                                        <input type="hidden" name="hash_algorithm" value="SHA256"/>
                                                        <input type="hidden" name="hash"
                                                               value="{{$hash}}"/>
                                                        <input type="hidden" name="storename" value="3900193"/>
                                                        <input type="hidden" name="mode" value="payonly"/>
                                                        <input type="hidden" name="checkoutoption"
                                                               value="combinedpage"/>
                                                        <input type="hidden" name="chargetotal"
                                                               value="{{$totalTrans}}"/>
                                                        <input type="hidden" name="currency" value="484"/>
                                                        <input type="hidden" name="oid"
                                                               value="{{$idTransac}}"/>
                                                        <input type="hidden" name="responseSuccessURL"
                                                               value="https://payment.sneakerstore.com.mx/thanks"/>
                                                        <input type="hidden" name="responseFailURL"
                                                               value="https://payment.sneakerstore.com.mx/fail"/>
                                                        <input type="submit" value="Pago en una sola exhibición"
                                                               class="btn btn-primary btn-sm">
                                                    </form>
                                                </div>
                                                <div class="row mt-3">
                                                    <button class="btn btn-sm btn-primary" onclick="mostrarMeses()">Pago
                                                        a
                                                        meses sin intereses
                                                    </button>
                                                    <div id="formMeses" style="display: none;">
                                                        <form method="post"
                                                              action="https://www2.ipg-online.com/connect/gateway/processing">
                                                            <input type="hidden" name="txntype" value="sale">
                                                            <input type="hidden" name="timezone"
                                                                   value="America/Mexico_City"/>
                                                            <input type="hidden" name="txndatetime"
                                                                   value="{{$dateTime}}"/>
                                                            <input type="hidden" name="hash_algorithm" value="SHA256"/>
                                                            <input type="hidden" name="hash"
                                                                   value="{{$hash}}"/>
                                                            <input type="hidden" name="storename" value="3900193"/>
                                                            <input type="hidden" name="mode" value="payonly"/>
                                                            <input type="hidden" name="checkoutoption"
                                                                   value="combinedpage"/>
                                                            <input type="hidden" name="chargetotal"
                                                                   value="{{$totalTrans}}"/>
                                                            <input type="hidden" name="currency" value="484"/>
                                                            <input type="hidden" name="oid"
                                                                   value="{{$idTransac}}"/>
                                                            <div class="form-group">
                                                                <label for="meses">Escoja un plazo</label>
                                                                <select name="installmentDelayMonths" id="meses"
                                                                        class="form-control">
                                                                    <option value="3">3 meses</option>
                                                                    @if($totalTrans >= 8000)
                                                                        <option value="6">6 meses</option>
                                                                    @endif
                                                                </select>
                                                            </div>
                                                            <input type="hidden" name="responseSuccessURL"
                                                                   value="http://localhost:8001/thanks"/>
                                                            <input type="hidden" name="responseFailURL"
                                                                   value="http://localhost:8001/fail"/>
                                                            <input type="submit" value="Continuar"
                                                                   class="btn btn-primary btn-sm">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <p>
                                        <i class="fas fa-lock"></i>
                                        Esta es una pagina segura que utiliza SSL/TLS (Secure Socket Layer/
                                        Transport Layer Security) para cifrar y transmitir sus datos de pago de una
                                        forma
                                        segura.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function mostrarMeses() {
            document.getElementById("formMeses").style.display = "block";
        }
    </script>
@endsection
